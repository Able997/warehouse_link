﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class StudentScore : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnDelete.Attributes.Add("OnClick", "return checkDelete('Singlechk');");
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindStudentScore(gvStudentScore.PageSize, gvStudentScore.PageIndex);
            }
        }

        private void BindStudentScore(int pagesize, int pageindex)
        {
            string sql0 = "select COUNT(a.Id) from tb_Score a join tb_StuInfo b on a.StuId=b.Id join tb_ClassInfo c on c.Id= b.ClassId join tb_Subject d on d.Id=a.SubjectId join tb_Schedule e on a.ScheduleId=e.Id where b.StuNum like @StuNum and b.StuName like @StuName and c.ClassName like @ClassName and d.SubjectName like @SubjectName and e.ScheduleName like @ScheduleName";
            AspNetPager1.AlwaysShow = true;
            AspNetPager1.PageSize = pagesize;

            string sql = "select top " + pagesize + "  a.*,b.StuNum,b.StuName,c.ClassName,d.SubjectName,e.ScheduleName from tb_Score a join tb_StuInfo b on a.StuId=b.Id join tb_ClassInfo c on c.Id= b.ClassId join tb_Subject d on d.Id=a.SubjectId join tb_Schedule e on a.ScheduleId=e.Id where b.StuNum like @StuNum and b.StuName like @StuName and c.ClassName like @ClassName and d.SubjectName like @SubjectName and e.ScheduleName like @ScheduleName and a.Id not in(select top " + (pageindex - 1) * pagesize + " Id from tb_Score)";
            SqlParameter[] parameters =
                {                          
                    new SqlParameter("@StuNum","%"+txtStuNum.Text.Trim()+"%"),   
                    new SqlParameter("@StuName","%"+txtName.Text.Trim()+"%"),
                    new SqlParameter("@ClassName","%"+txtClass.Text.Trim()+"%"),
                    new SqlParameter("@SubjectName","%"+txtSubject.Text.Trim()+"%"),
                    new SqlParameter("@ScheduleName","%"+txtSchedule.Text.Trim()+"%")
                };
            AspNetPager1.RecordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql0, parameters));
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql, parameters);
            gvStudentScore.DataSource = ds;
            gvStudentScore.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentScoreAdd.aspx");
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvStudentScore.PageIndex = AspNetPager1.CurrentPageIndex;
            BindStudentScore(gvStudentScore.PageSize, gvStudentScore.PageIndex);
        }

        protected void gvStudentScore_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindStudentScore(gvStudentScore.PageSize, gvStudentScore.PageIndex);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string sql = "delete from tb_Score where Id in (" + txtScore.Value + ")";
            bool result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
            if (result)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                BindStudentScore(gvStudentScore.PageSize, gvStudentScore.PageIndex);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
            }
        }

        protected void btnOut_Click(object sender, EventArgs e)
        {
            string sql = "select a.*,b.StuNum,b.StuName,c.ClassName,d.SubjectName,e.ScheduleName from tb_Score a join tb_StuInfo b on a.StuId=b.Id join tb_ClassInfo c on c.Id= b.ClassId join tb_Subject d on d.Id=a.SubjectId join tb_Schedule e on a.ScheduleId=e.Id where b.StuNum like @StuNum and b.StuName like @StuName and c.ClassName like @ClassName and d.SubjectName like @SubjectName and e.ScheduleName like @ScheduleName";
            SqlParameter[] parameters =
                {                          
                    new SqlParameter("@StuNum","%"+txtStuNum.Text.Trim()+"%"),   
                    new SqlParameter("@StuName","%"+txtName.Text.Trim()+"%"),
                    new SqlParameter("@ClassName","%"+txtClass.Text.Trim()+"%"),
                    new SqlParameter("@SubjectName","%"+txtSubject.Text.Trim()+"%"),
                    new SqlParameter("@ScheduleName","%"+txtSchedule.Text.Trim()+"%")
                };
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql, parameters);
            LendOutExcel("Score", ds.Tables[0]);
        }

        public bool LendOutExcel(string strFileName, DataTable DT)
        {
            try
            {
                //清除Response缓存内容
                HttpContext.Current.Response.Clear();
                //缓存输出，并在完成整个响应之后将其发送
                HttpContext.Current.Response.Buffer = true;
                //strFileName指定输出文件的名称，注意其扩展名和指定文件类型相符，可以为：.doc .xls .txt .htm
                strFileName = strFileName + ".xls";
                //设置输出流的HTPP字符集，确定字符的编码格式
                //HttpContext.Current.Response.Charset = "UTF-8";
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
                //下面这行很重要， attachment 参数表示作为附件下载，您可以改成 online在线打开 
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(strFileName));
                //Response.ContentType指定文件类型.可以为application/ms-excel,application/ms-word,application/ms-txt,application/ms-html或其他浏览器可直接支持文档
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                string colHeaders = "", ls_item = "";
                int i = 0;
                //定义表对象与行对像，同时用DataSet对其值进行初始化
                DataRow[] myRow = DT.Select("");
                //取得数据表各列标题，各标题之间以\t分割，最后一个列标题后加回车符
                for (i = 0; i < DT.Columns.Count - 1; i++)
                {
                    colHeaders += DT.Columns[i].Caption.ToString() + "\t";
                }
                colHeaders += DT.Columns[i].Caption.ToString() + "\n";
                //向HTTP输出流中写入取得的数据信息
                HttpContext.Current.Response.Write(colHeaders);
                //逐行处理数据 
                foreach (DataRow row in myRow)
                {
                    //在当前行中，逐列获得数据，数据之间以\t分割，结束时加回车符\n
                    for (i = 0; i < DT.Columns.Count - 1; i++)
                        ls_item += row[i].ToString() + "\t";
                    ls_item += row[i].ToString() + "\n";
                    //当前行数据写入HTTP输出流，并且置空ls_item以便下行数据    
                    HttpContext.Current.Response.Write(ls_item);
                    ls_item = "";
                }
                //写缓冲区中的数据到HTTP头文件中
                HttpContext.Current.Response.End();
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void gvStudentScore_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            if (e.CommandName == "upd")
            {
                Response.Redirect("StudentScoreAdd.aspx?id=" + id);
            }
            else
            {
                string sql = "delete from tb_Score where Id=" + id;
                bool result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                    BindStudentScore(gvStudentScore.PageSize, gvStudentScore.PageIndex);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
                }
            }
        }
    }
}