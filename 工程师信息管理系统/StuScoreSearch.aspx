﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StuScore.Master" AutoEventWireup="true" CodeBehind="StuScoreSearch.aspx.cs" Inherits="SMSystem.StuScoreSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width: 100%; height: 490px;">
        <br />
        <br />
        <br />
        <table style="width: 800px; margin: auto; text-align: center;">
            <tr>
                <td>工程期次：<asp:DropDownList ID="ddlSchedule" runat="server" Width="150px"></asp:DropDownList>&nbsp; &nbsp; &nbsp; &nbsp;工程名称：<asp:DropDownList ID="ddlSubject" runat="server" Width="150px"></asp:DropDownList>&nbsp; &nbsp; &nbsp; &nbsp;<asp:Button ID="btnSearch" runat="server" Text="查 询" OnClick="btnSearch_Click" />&nbsp; &nbsp; &nbsp; &nbsp;<a href="StuUpdatePwd.aspx" style="color: #165705">密码修改</a></td>

            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="labLoginer" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:GridView ID="gvScore" runat="server" Width="80%" AllowPaging="true" AutoGenerateColumns="false" CssClass="gvScore" OnPageIndexChanging="gvScore_PageIndexChanging" OnRowDataBound="gvScore_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="工程期次">
                                <ItemTemplate>
                                    <%#Eval("ScheduleName") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="工程名称">
                                <ItemTemplate>
                                    <%#Eval("SubjectName") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="工资">
                                <ItemTemplate>
                                    <%#Eval("Score") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <div>没有数据！</div>
                        </EmptyDataTemplate>

                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
