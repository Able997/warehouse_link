﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StuScoreCount.aspx.cs" Inherits="SMSystem.StuScoreCount" %>

<!DOCTYPE html>

<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }

        .STYLE1 {
            font-size: 12px;
        }

        .STYLE4 {
            font-size: 12px;
            color: #1F4A65;
            font-weight: bold;
        }

        a:link {
            font-size: 12px;
            color: #06482a;
            text-decoration: none;
        }

        a:visited {
            font-size: 12px;
            color: #06482a;
            text-decoration: none;
        }

        a:hover {
            font-size: 12px;
            color: #FF0000;
            text-decoration: underline;
        }

        a:active {
            font-size: 12px;
            color: #FF0000;
            text-decoration: none;
        }

        #gvScoreCount {
            width: 99%;
            border: 0px;
            text-align: center;
        }

        #gvScoreCount th {
            border: 0px;
            background-image: url("Images/tab/tab_14.gif");
        }

        #gvScoreCount td {
            border: 1px solid #FFFFFF;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td height="30">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="15">
                                <img src="Images/tab/tab_03.gif" width="15" height="30" /></td>
                            <td background="Images/tab/tab_05.gif">
                                <img src="Images/tab/311.gif" width="16" height="16" />
                                <span class="STYLE4">工程师工资统计</span>
                                <span style="float:right;">工程期次：<asp:DropDownList ID="ddlSchedule" Width="150px" runat="server"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;部门：<asp:DropDownList ID="ddlClass" Width="150px" runat="server"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSearch" runat="server" Text="查 询" OnClick="btnSearch_Click" /></span>
                            </td>
                            <td width="14">
                                <img src="Images/tab/tab_07.gif" width="14" height="30" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="9" background="Images/tab/tab_12.gif">&nbsp;</td>
                            <td bgcolor="e5f1d6">

                                <asp:GridView ID="gvScoreCount" runat="server" PageSize="10" PageIndex="1" AutoGenerateColumns="false" Width="100%" OnRowDataBound="gvScoreCount_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="工程名称">
                                            <ItemTemplate>
                                                <%#Eval("SubjectName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="总工资">
                                            <ItemTemplate>
                                                <%#Eval("SumScore") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="平均工资">
                                            <ItemTemplate>
                                                <%#Convert.ToDouble(Eval("AvgScore")).ToString("0.00") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="人数">
                                            <ItemTemplate>
                                                <%#Eval("PersonCount") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        
                                    </Columns>
                                </asp:GridView>


                            </td>
                            <td width="9" background="Images/tab/tab_16.gif">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="29">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="15" height="29">
                                <img src="Images/tab/tab_20.gif" width="15" height="29" /></td>
                            <td background="Images/tab/tab_21.gif">
 &nbsp;
                            </td>
                            <td width="14">
                                <img src="Images/tab/tab_22.gif" width="14" height="29" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
