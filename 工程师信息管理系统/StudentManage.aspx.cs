﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace SMSystem
{
    public partial class StudentManage : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnDelete.Attributes.Add("OnClick", "return checkDelete('Singlechk');");
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindStudents(gvStudentManage.PageSize, gvStudentManage.PageIndex);
            }
        }

        private void BindStudents(int pagesize, int pageindex)
        {
            if (txtJoinTime.Value.Length == 0)
            {
                string sql0 = "select count(*) from tb_StuInfo where StuNum like @StuNum and StuName like @StuName and datediff(year,Birthday,getdate()) like @Age and (select ClassName from tb_ClassInfo where Id=tb_StuInfo.ClassId) like @ClassName";
                AspNetPager1.AlwaysShow = true;
                AspNetPager1.PageSize = pagesize;

                string sql = "select top " + pagesize + " a.*,datediff(year,Birthday,getdate()) as Age,b.ClassName from tb_StuInfo a join tb_ClassInfo b on a.ClassId=b.Id where a.StuNum like @StuNum and a.StuName like @StuName and  b.ClassName like @ClassName  and datediff(year,Birthday,getdate()) like @Age and  a.Id not in(select top " + (pageindex - 1) * pagesize + " Id from tb_StuInfo)";

                SqlParameter[] parameters =
                {                          
                    new SqlParameter("@StuNum","%"+txtId.Text.Trim()+"%"),   
                    new SqlParameter("@StuName","%"+txtName.Text.Trim()+"%"),
                    new SqlParameter("@Age","%"+txtAge.Text.Trim()+"%"),
                    new SqlParameter("@ClassName","%"+txtClass.Text.Trim()+"%")
                };
                AspNetPager1.RecordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql0, parameters));
                DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql, parameters);
                gvStudentManage.DataSource = ds;
                gvStudentManage.DataBind();
            }
            else
            {
                string sql0 = "select count(*) from tb_StuInfo where StuNum like @StuNum and StuName like @StuName and datediff(year,Birthday,getdate()) like @Age and (select ClassName from tb_ClassInfo where Id=tb_StuInfo.ClassId) like @ClassName and StartTime=@StartTime";
                AspNetPager1.AlwaysShow = true;
                AspNetPager1.PageSize = pagesize;

                string sql = "select top " + pagesize + " a.*,datediff(year,Birthday,getdate()) as Age,b.ClassName from tb_StuInfo a join tb_ClassInfo b on a.ClassId=b.Id where a.StuNum like @StuNum and a.StuName like @StuName and  b.ClassName like @ClassName  and datediff(year,Birthday,getdate()) like @Age and StartTime=@StartTime and  a.Id not in(select top " + (pageindex - 1) * pagesize + " Id from tb_StuInfo)";

                SqlParameter[] parameters =
                {                          
                    new SqlParameter("@StuNum","%"+txtId.Text.Trim()+"%"),   
                    new SqlParameter("@StuName","%"+txtName.Text.Trim()+"%"),
                    new SqlParameter("@Age","%"+txtAge.Text.Trim()+"%"),
                    new SqlParameter("@ClassName","%"+txtClass.Text.Trim()+"%"),
                    new SqlParameter("@StartTime",txtJoinTime.Value.Trim())
                };
                AspNetPager1.RecordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql0, parameters));
                DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql, parameters);
                gvStudentManage.DataSource = ds;
                gvStudentManage.DataBind();
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvStudentManage.PageIndex = AspNetPager1.CurrentPageIndex;
            BindStudents(gvStudentManage.PageSize, gvStudentManage.PageIndex);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentAdd.aspx");
        }

        protected void gvStudentManage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            if (e.CommandName == "upd")
            {
                Response.Redirect("StudentAdd.aspx?Id=" + id);
            }
            else
            {
                string sql = "delete from tb_StuInfo where Id=" + id;
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
                if (result)
                {
                    try
                    {
                        //删除学生信息后，也删除对应成绩
                        SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, "delete from tb_Score where StuId=" + id);
                    }
                    catch
                    {
                    }
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                    BindStudents(gvStudentManage.PageSize, gvStudentManage.PageIndex);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
                }
            }
        }

        protected void gvStudentManage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindStudents(gvStudentManage.PageSize, 1);
            AspNetPager1.CurrentPageIndex = 1;
        }

        protected void btnOut_Click(object sender, EventArgs e)
        {
            if (txtJoinTime.Value.Length == 0)
            {
                string sql = "select  a.StuNum as 学号,a.StuName as 姓名,datediff(year,Birthday,getdate()) as 年龄,case a.Sex when 0 then '女' else '男' end as 性别,a.StartTime as 入学时间, b.ClassName as 班级 from tb_StuInfo a join tb_ClassInfo b on a.ClassId=b.Id where a.StuNum like @StuNum and a.StuName like @StuName and  b.ClassName like @ClassName  and datediff(year,Birthday,getdate()) like @Age ";

                SqlParameter[] parameters =
                {                          
                    new SqlParameter("@StuNum","%"+txtId.Text.Trim()+"%"),   
                    new SqlParameter("@StuName","%"+txtName.Text.Trim()+"%"),
                    new SqlParameter("@Age","%"+txtAge.Text.Trim()+"%"),
                    new SqlParameter("@ClassName","%"+txtClass.Text.Trim()+"%")
                };
                DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql, parameters);
                LendOutExcel("student", ds.Tables[0]);
            }
            else
            {
                string sql = "select  a.StuNum as 学号,a.StuName as 姓名,datediff(year,Birthday,getdate()) as 年龄,case a.Sex when 0 then '女' else '男' end as 性别,a.StartTime as 入学时间, b.ClassName as 班级 from tb_StuInfo a join tb_ClassInfo b on a.ClassId=b.Id where a.StuNum like @StuNum and a.StuName like @StuName and  b.ClassName like @ClassName  and datediff(year,Birthday,getdate()) like @Age and a.StartTime=@StartTime";

                SqlParameter[] parameters =
                {                          
                    new SqlParameter("@StuNum","%"+txtId.Text.Trim()+"%"),   
                    new SqlParameter("@StuName","%"+txtName.Text.Trim()+"%"),
                    new SqlParameter("@Age","%"+txtAge.Text.Trim()+"%"),
                    new SqlParameter("@ClassName","%"+txtClass.Text.Trim()+"%"),
                    new SqlParameter("@StartTime",txtJoinTime.Value.Trim())
                };
                DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql, parameters);
                LendOutExcel("student", ds.Tables[0]);
            }
        }

        public bool LendOutExcel(string strFileName, DataTable DT)
        {
            try
            {
                //清除Response缓存内容
                HttpContext.Current.Response.Clear();
                //缓存输出，并在完成整个响应之后将其发送
                HttpContext.Current.Response.Buffer = true;
                //strFileName指定输出文件的名称，注意其扩展名和指定文件类型相符，可以为：.doc .xls .txt .htm
                strFileName = strFileName + ".xls";
                //设置输出流的HTPP字符集，确定字符的编码格式
                //HttpContext.Current.Response.Charset = "UTF-8";
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
                //下面这行很重要， attachment 参数表示作为附件下载，您可以改成 online在线打开 
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(strFileName));
                //Response.ContentType指定文件类型.可以为application/ms-excel,application/ms-word,application/ms-txt,application/ms-html或其他浏览器可直接支持文档
                HttpContext.Current.Response.ContentType = "application/ms-excel";
                string colHeaders = "", ls_item = "";
                int i = 0;
                //定义表对象与行对像，同时用DataSet对其值进行初始化
                DataRow[] myRow = DT.Select("");
                //取得数据表各列标题，各标题之间以\t分割，最后一个列标题后加回车符
                for (i = 0; i < DT.Columns.Count - 1; i++)
                {
                    colHeaders += DT.Columns[i].Caption.ToString() + "\t";
                }
                colHeaders += DT.Columns[i].Caption.ToString() + "\n";
                //向HTTP输出流中写入取得的数据信息
                HttpContext.Current.Response.Write(colHeaders);
                //逐行处理数据 
                foreach (DataRow row in myRow)
                {
                    //在当前行中，逐列获得数据，数据之间以\t分割，结束时加回车符\n
                    for (i = 0; i < DT.Columns.Count - 1; i++)
                        ls_item += row[i].ToString() + "\t";
                    ls_item += row[i].ToString() + "\n";
                    //当前行数据写入HTTP输出流，并且置空ls_item以便下行数据    
                    HttpContext.Current.Response.Write(ls_item);
                    ls_item = "";
                }
                //写缓冲区中的数据到HTTP头文件中
                HttpContext.Current.Response.End();
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string sql = "delete from tb_StuInfo where Id in (" + txtStu.Value + ")";
            bool result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
            if (result)
            {
                try
                {
                    //删除学生信息后，也删除对应成绩
                    SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, "delete from tb_Score where StuId in ("+txtStu.Value+")");
                }
                catch
                {
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                BindStudents(gvStudentManage.PageSize, gvStudentManage.PageIndex);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
            }
        }
    }
}