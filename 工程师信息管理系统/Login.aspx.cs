﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class Login : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ibtnLogin_Click(object sender, ImageClickEventArgs e)
        {
            //管理员登录
            if (radTeacher.Checked)
            {
                string user = this.txtUserName.Text;
                string pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(this.txtPwd.Text, "MD5");
                string sql = "select a.*,b.RoleName,b.PowerIds from tb_UserInfo a join tb_Role b on a.RoleId=b.Id where a.LoginName=@LoginName and a.LoginPwd=@LoginPwd";
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@LoginName",user),
                   new SqlParameter("@LoginPwd",pwd)
                };
                SqlDataReader sdr = SqlHelper.ExecuteReader(connStr, CommandType.Text, sql, parameters);
                if (sdr.Read())
                {
                    Session["UserName"] = sdr["UserName"].ToString();
                    Session["UserId"] = sdr["Id"].ToString();
                    Session["RoleName"] = sdr["RoleName"].ToString();
                    sdr.Close();
                    Response.Redirect("Main.html");
                }
                else
                {
                    sdr.Close();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "loginError", "alert('用户或密码错误！');", true);
                }
            }
            //工程师登录
            else
            {
                string sql = "select * from tb_StuInfo where StuNum=@StuNum and StuPwd=@StuPwd";
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@StuNum",txtUserName.Text.Trim()),
                   new SqlParameter("@StuPwd",txtPwd.Text.Trim())
                };
                SqlDataReader sdr = SqlHelper.ExecuteReader(connStr, CommandType.Text, sql, parameters);
                if (sdr.Read())
                {
                    Response.Cookies["StuId"].Value = sdr["Id"].ToString();
                    Response.Cookies["StuId"].Expires = DateTime.Now.AddMinutes(5);

                    Response.Cookies["StuName"].Value =HttpUtility.UrlEncode(sdr["StuName"].ToString());
                    Response.Cookies["StuName"].Expires = DateTime.Now.AddMinutes(5);

                    sdr.Close();
                    Response.Redirect("StuScoreSearch.aspx");
                }
                else
                {
                    sdr.Close();
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "loginError", "alert('用户或密码错误！');", true);
                }

            }
        }
    }
}