﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class ScheduleManage : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindGridView(gvScheduleManage.PageSize,gvScheduleManage.PageIndex);
            }
        }

        private void BindGridView(int pagesize, int pageindex)
        {
            string sql0 = "select COUNT(*) from tb_Schedule";
            AspNetPager1.AlwaysShow = true;
            AspNetPager1.PageSize = pagesize;

            string sql = "select top " + pagesize + " * from tb_Schedule where Id not in(select top " + (pageindex - 1) * pagesize + " Id from tb_Schedule)";
            AspNetPager1.RecordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql0));
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            gvScheduleManage.DataSource = ds;
            gvScheduleManage.DataBind();
        }

        protected void gvScheduleManage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void gvScheduleManage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            if (e.CommandName == "upd")
            {
                Response.Redirect("ScheduleAdd.aspx?Id=" + id);
            }
            else
            {
                //判断该期次下是否存在成绩
                string chkSql = "select ScheduleId from tb_Score where ScheduleId=" + id;
                object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, chkSql);
                if (obj != null)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "havestu", "alert('存在该期次的成绩，不能删除该期次！')", true);
                    return;
                }

                string sql = "delete from tb_Schedule where Id=" + id;
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                    BindGridView(gvScheduleManage.PageSize, gvScheduleManage.PageIndex);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
                }
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvScheduleManage.PageIndex = AspNetPager1.CurrentPageIndex;
            BindGridView(gvScheduleManage.PageSize, gvScheduleManage.PageIndex);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("ScheduleAdd.aspx");
        }
    }
}