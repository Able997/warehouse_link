﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class Top : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserName"] == null)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "loginError", "parent.location = 'Login.aspx';", true);
                }
                else
                {
                    labelUser.Text = Session["UserName"].ToString();
                    labelGroup.Text = Session["RoleName"].ToString();
                }
            }
        }

        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            Session["UserName"] = null;
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "loginError", "parent.location = 'Login.aspx';", true);
        }
    }
}