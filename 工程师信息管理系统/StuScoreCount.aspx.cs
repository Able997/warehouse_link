﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class StuScoreCount : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindSchedule();
                BindClass();
                BindScoreCount(gvScoreCount.PageSize,gvScoreCount.PageIndex);
            }
        }

        private void BindClass()
        {
            string sql = "select * from tb_ClassInfo";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            ddlClass.DataValueField = "Id";
            ddlClass.DataTextField = "ClassName";
            ddlClass.DataSource = ds;
            ddlClass.DataBind();
            ddlClass.Items.Insert(0, new ListItem("  ", "0"));
        }

        private void BindSchedule()
        {
            string sql = "select * from tb_Schedule";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            ddlSchedule.DataValueField = "Id";
            ddlSchedule.DataTextField = "ScheduleName";
            ddlSchedule.DataSource = ds;
            ddlSchedule.DataBind();
            ddlSchedule.Items.Insert(0, new ListItem("  ", "0"));
        }

        private void BindScoreCount(int pagesize, int pageindex)
        {
            StringBuilder str = new StringBuilder();
            str.Append("select SubjectName,sum(Score) as SumScore,avg(Score) as AvgScore,count(SubjectName) as PersonCount from View_StuScore");
            
            if (ddlClass.SelectedIndex > 0 && ddlSchedule.SelectedIndex>0)
            {
                str.Append(" where ClassId="+ddlClass.SelectedValue+" and ScheduleId="+ddlSchedule.SelectedValue);
            }
            else if (ddlClass.SelectedIndex > 0 && ddlSchedule.SelectedIndex == 0)
            {
                str.Append(" where ClassId=" + ddlClass.SelectedValue);
            }
            else if (ddlClass.SelectedIndex == 0 && ddlSchedule.SelectedIndex > 0)
            {
                str.Append(" where ScheduleId=" + ddlSchedule.SelectedValue);
            }
            str.Append(" group by SubjectName");
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, str.ToString());
            gvScoreCount.DataSource = ds;
            gvScoreCount.DataBind();
        }

        protected void gvScoreCount_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindScoreCount(gvScoreCount.PageSize, gvScoreCount.PageIndex);
        }

    }
}