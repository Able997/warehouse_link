﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SMSystem.Login" %>

<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>工程师信息管理系统_用户登录</title>
    <style type="text/css">
        
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            overflow: hidden;
        }

        .STYLE1 {
            font-size: 12px;
            color:#333131;
        }
        .txtstyle {
            height: 17px;
            border: solid 1px #bbbbbb;
        }
    </style>
</head>

<body>
    <form id="form1" runat="server">
    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td bgcolor="9fc967">&nbsp;</td>
        </tr>
        <tr>
            <td height="604">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="604" background="Images/login_02.gif">&nbsp;</td>
                        <td width="989">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="345" background="Images/login1.jpg">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="47">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="539" height="47" background="Images/login_05.gif" nowrap="nowrap">&nbsp;</td>
                                                <td width="206" background="Images/login_06.gif" nowrap="nowrap">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="17%" height="22">
                                                                <div align="center" class="STYLE1">用户</div>
                                                            </td>
                                                            <td width="58%" height="22">
                                                                <div align="center">
                                                                    <asp:TextBox ID="txtUserName" Width="110px" runat="server" CssClass="txtstyle"></asp:TextBox>
                                                                </div>
                                                            </td>
                                                            <td width="25%" height="22">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td height="22">
                                                                <div align="center" class="STYLE1">密码</div>
                                                            </td>
                                                            <td height="22">
                                                                <div align="center">
                                                                    <asp:TextBox ID="txtPwd" Width="110px" TextMode="Password" runat="server" CssClass="txtstyle"></asp:TextBox>
                                                                </div>
                                                            </td>
                                                            <td height="22">
                                                                <div align="center">
                                                                    <asp:ImageButton ID="ibtnLogin" runat="server" ImageUrl="Images/dl.gif" OnClick="ibtnLogin_Click"/></div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="244" background="Images/login_07.gif" nowrap="nowrap" class="STYLE1">&nbsp;
                                                    <asp:RadioButton ID="radTeacher" runat="server" Checked="true" GroupName="rad"/>管理层
                                                    <asp:RadioButton ID="RadioButton1" runat="server" Checked="true" GroupName="rad"/>工程师
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="212" background="Images/login_08.gif">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td background="Images/login_04.gif">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="70ad21">&nbsp;</td>
        </tr>
    </table>
        </form>
</body>
</html>


