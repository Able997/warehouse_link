﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class StudentAdd : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOk.Attributes.Add("OnClick", "return checkStuInfo();");
            if (!IsPostBack)
            {
                BindClass();
                if (Request["id"] != null)
                {
                    lableInfo.Text = "工程师信息修改";
                    btnOk.Text = "修 改";
                    txtstuPwd.Enabled = false;
                    BindStuInfo(Request["id"].ToString());
                }
            }
        }

        private void BindClass()
        {
            string sql = "select * from tb_ClassInfo";
            DataSet ds= SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            ddlClass.DataValueField = "Id";
            ddlClass.DataTextField = "ClassName";
            ddlClass.DataSource = ds;
            ddlClass.DataBind();
        }

        private void BindStuInfo(string id)
        {
            string sql = "select * from tb_StuInfo where Id=" + id ;
            SqlDataReader sdr= SqlHelper.ExecuteReader(connStr, CommandType.Text, sql);
            if (sdr.Read())
            {
                txtstuId.Text = sdr["StuNum"].ToString().Trim();
                txtstuName.Text = sdr["StuName"].ToString().Trim();
                if (sdr["Sex"].ToString() == "False")
                {
                    radMan.Checked = false;
                    radWoman.Checked = true;
                }
                ddlClass.SelectedValue = sdr["ClassId"].ToString();
                txtBirth.Value = Convert.ToDateTime(sdr["Birthday"]).ToString("yyyy-MM-dd");
                txtJionTime.Value =Convert.ToDateTime(sdr["StartTime"]).ToString("yyyy-MM-dd");
            }
            sdr.Close();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            //添加
            if (btnOk.Text == "添 加")
            {
                //查询工号是否重复
                if (!CheckStuId("add"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addcheckerror", "alert('该工号已存在！')", true);
                    return;
                }


                string sql = "insert into tb_StuInfo values(@StuNum,@StuPwd,@StuName,@Birthday,@Sex,@ClassId,@StartTime)";
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@StuNum",txtstuId.Text.Trim()),
                   new SqlParameter("@StuPwd",txtstuPwd.Text.Trim()),
                   new SqlParameter("@StuName",txtstuName.Text.Trim()),
                   new SqlParameter("@Birthday",txtBirth.Value),
                   new SqlParameter("@Sex",radMan.Checked),
                   new SqlParameter("@ClassId",Convert.ToInt32(ddlClass.SelectedValue)),
                   new SqlParameter("@StartTime",txtJionTime.Value)
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addOk", "alert('添加成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addError", "alert('添加失败！')", true);
                }
            }
            //修改
            else
            {
                //查询工号是否重复
                if (!CheckStuId("upd"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "updcheckerror", "alert('该工号已存在！')", true);
                    return;
                }

                string sql = "update tb_StuInfo set StuNum=@StuNum,StuName=@StuName,Birthday=@Birthday,Sex=@Sex,ClassId=@ClassId,StartTime=@StartTime where Id=" + Request["id"];
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@StuNum",txtstuId.Text.Trim()),
                   new SqlParameter("@StuName",txtstuName.Text.Trim()),
                   new SqlParameter("@Birthday",txtBirth.Value),
                   new SqlParameter("@Sex",radMan.Checked),
                   new SqlParameter("@ClassId",Convert.ToInt32(ddlClass.SelectedValue)),
                   new SqlParameter("@StartTime",txtJionTime.Value)
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editOk", "alert('修改成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editError", "alert('修改失败！')", true);
                }
            }
        }

        private bool CheckStuId(string p)
        {
            if (p == "add")
            {
                string sql = "select StuNum from tb_StuInfo where StuNum=@StuNum";
                SqlParameter parameter = new SqlParameter("@StuNum", txtstuId.Text.Trim());
                object obj= SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql, parameter);
                if (obj != null)
                {
                    return false;
                }
            }
            else
            {
                string sql = "select StuNum from tb_StuInfo where StuNum=@StuNum and Id<>" + Request["id"];
                SqlParameter parameter = new SqlParameter("@StuNum", txtstuId.Text.Trim());
                object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql, parameter);
                if (obj != null)
                {
                    return false;
                }
            }
            return true;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentManage.aspx");
        }
    }
}