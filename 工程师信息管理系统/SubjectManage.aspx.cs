﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class SubjectManage : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindGridView(gvSubjectManage.PageSize, gvSubjectManage.PageIndex);
            }
        }

        private void BindGridView(int pagesize, int pageindex)
        {
            string sql0 = "select COUNT(*) from tb_Subject";
            AspNetPager1.AlwaysShow = true;
            AspNetPager1.PageSize = pagesize;

            string sql = "select top " + pagesize + " * from tb_Subject where Id not in(select top " + (pageindex - 1) * pagesize + " Id from tb_Subject)";
            AspNetPager1.RecordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql0));
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            gvSubjectManage.DataSource = ds;
            gvSubjectManage.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("SubjectAdd.aspx");
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvSubjectManage.PageIndex = AspNetPager1.CurrentPageIndex;
            BindGridView(gvSubjectManage.PageSize, gvSubjectManage.PageIndex);
        }

        protected void gvSubjectManage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void gvSubjectManage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            if (e.CommandName == "upd")
            {
                Response.Redirect("SubjectAdd.aspx?Id=" + id);
            }
            else
            {
                //判断该科目下成绩
                string chkSql = "select SubjectId from tb_Score where SubjectId=" + id;
                object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, chkSql);
                if (obj != null)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "havestu", "alert('存在该科目下的成绩，不能删除该科目！')", true);
                    return;
                }

                string sql = "delete from tb_Subject where Id=" + id;
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                    BindGridView(gvSubjectManage.PageSize, gvSubjectManage.PageIndex);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
                }
            }
        }
    }
}