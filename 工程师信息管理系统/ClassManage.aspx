﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClassManage.aspx.cs" Inherits="SMSystem.ClassManage" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html>

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }

        .STYLE1 {
            font-size: 12px;
        }

        .STYLE4 {
            font-size: 12px;
            color: #1F4A65;
            font-weight: bold;
        }
        .toright {
            float:right;
        }

        a:link {
            font-size: 12px;
            color: #06482a;
            text-decoration: none;
        }

        a:visited {
            font-size: 12px;
            color: #06482a;
            text-decoration: none;
        }

        a:hover {
            font-size: 12px;
            color: #FF0000;
            text-decoration: underline;
        }

        a:active {
            font-size: 12px;
            color: #FF0000;
            text-decoration: none;
        }

        #gvClassManage {
            width: 99%;
            border: 0px;
            text-align: center;
        }

            #gvClassManage th {
                border: 0px;
                background-image: url("Images/tab/tab_14.gif");
            }

            #gvClassManage td {
                border: 1px solid #FFFFFF;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td height="30">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="15">
                                <img src="Images/tab/tab_03.gif" width="15" height="30" /></td>
                            <td background="Images/tab/tab_05.gif">
                                <img src="Images/tab/311.gif" width="16" height="16" />
                                <span class="STYLE4">部门信息管理</span>
                                    <asp:Button ID="btnAdd" runat="server" Text="新增信息" OnClick="btnAdd_Click" CssClass="toright" />
                            </td>
                            <td width="14">
                                <img src="Images/tab/tab_07.gif" width="14" height="30" /></td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="9" background="Images/tab/tab_12.gif">&nbsp;</td>
                            <td bgcolor="e5f1d6">

                                <asp:GridView ID="gvClassManage" runat="server" PageSize="10" AutoGenerateColumns="false" Width="100%" PageIndex="1" OnRowCommand="gvClassManage_RowCommand" OnRowDataBound="gvClassManage_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="部门名称">
                                            <ItemTemplate>
                                                <%#Eval("ClassName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="备注">
                                            <ItemTemplate>
                                                <%#Eval("Remark") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="操作" HeaderStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnUpdate" CommandName="upd" CommandArgument='<%#Eval("Id") %>' runat="server"><img src="Images/114.gif" style="border:0px;"/>编辑</asp:LinkButton>
                                                &nbsp;&nbsp;
                                            <asp:LinkButton ID="lbtnDelete" OnClientClick="return confirm('确定要删除吗？')" CommandName="del" CommandArgument='<%#Eval("Id")  %>' runat="server"><img src="Images/114.gif" style="border:0px;"/>删除</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>


                            </td>
                            <td width="9" background="Images/tab/tab_16.gif">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="29">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="15" height="29">
                                <img src="Images/tab/tab_20.gif" width="15" height="29" /></td>
                            <td background="Images/tab/tab_21.gif">

                                <webdiyer:AspNetPager runat="server" ID="AspNetPager1" runat="server" HorizontalAlign="Right" FirstPageText="<<" LastPageText=">>" PrevPageText="<" NextPageText=">" NumericButtonTextFormatString=" {0} " Width="600px"
                                    ShowCustomInfoSection="Left" ShowBoxThreshold="2" PageSize="5" InputBoxClass="text2" TextAfterInputBox="" OnPageChanged="AspNetPager1_PageChanged">
                                </webdiyer:AspNetPager>
                            </td>
                            <td width="14">
                                <img src="Images/tab/tab_22.gif" width="14" height="29" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
