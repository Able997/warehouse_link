﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PowerAdd.aspx.cs" Inherits="SMSystem.PowerAdd" %>

<!DOCTYPE html>

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        body {
            background-color: #F2FFE2;
        }

        .tableframe {
            margin-left: 4px;
            width: 346px;
            border: solid #add9c0;
            border-width: 1px 0px 0px 1px;
        }

            .tableframe td {
                border: solid #add9c0;
                border-width: 0px 1px 1px 0px;
                padding: 5px 0px;
            }

        .tdleft {
            width: 100px;
            text-align: right;
            background-color: #EBFBD6;
            color: #1F4A65;
        }

        .tablepower {
            margin: auto;
            width: 80%;
            /*text-align:center;*/
            border: 1px solid #add9c0;
        }

            .tablepower td {
                padding-left: 60px;
            }
    </style>
    <script type="text/javascript">
        function checkGroup(checkBoxName) {
            var xx = "";
            var checkBox = document.getElementsByName(checkBoxName);
            for (var i = 0; i < checkBox.length; i++) {
                if (checkBox[i].checked) {
                    xx += checkBox[i].value + ",";
                }
            }
            document.getElementById('txtPower').value = xx;
            return true;
        }

        //全选
        function checkAll(checkBoxName) {
            var checkBox = document.getElementsByName(checkBoxName);
            for (var i = 0; i < checkBox.length; i++) {
                checkBox[i].checked = document.getElementById("chkAll").checked;
            }
        }

        function getCheck(checkBoxName) {
            var xx = "";
            var checkBox = document.getElementsByName(checkBoxName);
            for (var i = 0; i < checkBox.length; i++) {
                if (checkBox[i].checked) {
                    xx += checkBox[i].value + ",";
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" style="width: 350px; margin: 80px auto;" cellpadding="0" border="0" cellspacing="0">
                <tr>
                    <td style="background-image: url(Images/tab/tab_03.gif); height: 30px; width: 11px;"></td>
                    <td style="background-image: url(Images/tab/tab_05.gif); width: 350px; height: 30px; font-size: 12px; color: #1F4A65; font-weight: bold;">
                        <img src="Images/tab/311.gif" width="16" height="16" />&nbsp;&nbsp;<asp:Label ID="lableInfo" runat="server" Text="角色权限添加"></asp:Label>

                    </td>
                    <td style="background-image: url(Images/tab/tab_07.gif); height: 30px; width: 11px;"></td>
                </tr>
                <tr>

                    <td colspan="3">
                        <table class="tableframe" cellpadding="0" cellspacing="0">

                            <tr>
                                <td class="tdleft">角色名称：</td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox> 
                                </td>
                            </tr>
                            <tr>
                                <td class="tdleft">角色说明：</td>
                                <td>
                                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" MaxLength="100" Width="150px"></asp:TextBox>
                                </td>
                            </tr>


                        </table>
                    </td>
                </tr>
            </table>
            <div style="width:80%;margin:auto;">
                <input id="chkAll" type="checkbox" onclick="checkAll('powerchk')"/>全选
                <input id="txtPower" type="hidden" runat="server"/>
            </div>
            <br />
            <div id="divpower" runat="server">
                
            </div>
            <div style="text-align: center;">
                <br />
                <asp:Button ID="btnOk" runat="server" Text="添 加" OnClick="btnOk_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnReturn" runat="server" Text="返 回" OnClick="btnReturn_Click" />
            </div>
        </div>
    </form>
</body>
</html>
