﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class ClassManage : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"]==null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindgvClass(gvClassManage.PageSize,gvClassManage.PageIndex);
            }
        }

        private void BindgvClass(int pagesize, int pageindex)
        {
            string sql0 = "select COUNT(*) from tb_ClassInfo";
            AspNetPager1.AlwaysShow = true;
            AspNetPager1.PageSize = pagesize;

            string sql = "select top " + pagesize + " * from tb_ClassInfo where Id not in(select top " + (pageindex - 1) * pagesize + " Id from tb_ClassInfo)";
            AspNetPager1.RecordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql0));
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            gvClassManage.DataSource = ds;
            gvClassManage.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("ClassAdd.aspx");
        }

        protected void gvClassManage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void gvClassManage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            if (e.CommandName == "upd")
            {
                Response.Redirect("ClassAdd.aspx?Id=" + id);
            }
            else
            {
                //判断该部门下工程师
                string chkSql = "select ClassId from tb_StuInfo where ClassId=" + id;
                object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, chkSql);
                if (obj != null)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "havestu", "alert('存在该部门的工程师，不能删除该部门！')", true);
                    return;
                }

                string sql = "delete from tb_ClassInfo where Id=" + id;
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                    BindgvClass(gvClassManage.PageSize, gvClassManage.PageIndex);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
                }
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvClassManage.PageIndex = AspNetPager1.CurrentPageIndex;
            BindgvClass(gvClassManage.PageSize, gvClassManage.PageIndex);
        }
    }
}