﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClassAdd.aspx.cs" Inherits="SMSystem.ClassAdd" ValidateRequest="false" %>

<!DOCTYPE html>


<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style>
        body {
            background-color: #F2FFE2;
        }

        .tableframe {
            margin-left: 4px;
            width: 346px;
            border: solid #add9c0;
            border-width: 1px 0px 0px 1px;
        }

            .tableframe td {
                border: solid #add9c0;
                border-width: 0px 1px 1px 0px;
                padding: 5px 0px;
            }

        .tdleft {
            width: 100px;
            text-align: right;
            background-color: #EBFBD6;
            color: #1F4A65;
        }
    </style>
    <script type="text/javascript">
        function checkClassInfo() {
            if (document.getElementById("<%=txtClassName.ClientID%>").value == "" || document.getElementById("<%=txtClassName.ClientID%>").value.indexOf(" ") != -1) {
                alert('部门名不能为空！');
                return false;
            }

            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" style="width: 350px; margin: 80px auto;" cellpadding="0" border="0" cellspacing="0">
                <tr>
                    <td style="background-image: url(Images/tab/tab_03.gif); height: 30px; width: 11px;"></td>
                    <td style="background-image: url(Images/tab/tab_05.gif); width: 350px; height: 30px; font-size: 12px; color: #1F4A65; font-weight: bold;">
                        <img src="Images/tab/311.gif" width="16" height="16" />&nbsp;&nbsp;<asp:Label ID="lableInfo" runat="server" Text="部门信息添加"></asp:Label>

                    </td>
                    <td style="background-image: url(Images/tab/tab_07.gif); height: 30px; width: 11px;"></td>
                </tr>
                <tr>

                    <td colspan="3">
                        <table class="tableframe" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tdleft">部门名称：</td>
                                <td>
                                    <asp:TextBox ID="txtClassName" runat="server" MaxLength="20"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="tdleft">备注：</td>
                                <td>
                                    <asp:TextBox ID="txtRemark" runat="server" MaxLength="100" TextMode="MultiLine" Width="150px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnOk" runat="server" Text="添 加" OnClick="btnOk_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnReturn" runat="server" Text="返 回" OnClick="btnReturn_Click" />
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>
