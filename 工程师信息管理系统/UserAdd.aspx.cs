﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class UserAdd : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOk.Attributes.Add("OnClick", "return checkUserInfo();");
            if (!IsPostBack)
            {
                BindRole();
                if (Request["id"] != null)
                {
                    lableInfo.Text = "用户信息修改";
                    btnOk.Text = "修 改";
                    txtPwd.Enabled = false;
                    txtRPwd.Enabled = false;
                    BindUserInfo(Request["id"].ToString());
                }
            }
        }

        private void BindUserInfo(string id)
        {
            string sql = "select * from tb_UserInfo where Id=" + id;
            SqlDataReader sdr = SqlHelper.ExecuteReader(connStr, CommandType.Text, sql);
            if (sdr.Read())
            {
                txtLoginName.Text = sdr["LoginName"].ToString();
                txtUserName.Text = sdr["UserName"].ToString();
                txtPwd.Attributes.Add("Value", "*********");
                txtRPwd.Attributes.Add("Value", "*********");
                ddlRole.SelectedValue = sdr["RoleId"].ToString();
            }
            sdr.Close();
        }

        private void BindRole()
        {
            string sql = "select * from tb_Role";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            ddlRole.DataValueField = "Id";
            ddlRole.DataTextField = "RoleName";
            ddlRole.DataSource = ds;
            ddlRole.DataBind();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            //添加
            if (btnOk.Text == "添 加")
            {
                //查询登录名是否重复
                if (!CheckLoginName("add"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addcheckerror", "alert('该登录名已存在！')", true);
                    return;
                }


                string sql = "insert into tb_UserInfo values(@LoginName,@LoginPwd,@UserName,@RoleId)";
                string pwd = FormsAuthentication.HashPasswordForStoringInConfigFile(this.txtPwd.Text.Trim(), "MD5");
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@LoginName",txtLoginName.Text.Trim()),
                   new SqlParameter("@LoginPwd",pwd),
                   new SqlParameter("@UserName",txtUserName.Text.Trim()),
                   new SqlParameter("@RoleId",Convert.ToInt32(ddlRole.SelectedValue))
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addOk", "alert('添加成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addError", "alert('添加失败！')", true);
                }
            }
            //修改
            else
            {
                //查询登录名是否重复
                if (!CheckLoginName("upd"))
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "updcheckerror", "alert('该登录名已存在！')", true);
                    return;
                }

                string sql = "update tb_UserInfo set LoginName=@LoginName,UserName=@UserName,RoleId=@RoleId where Id=" + Request["id"];
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@LoginName",txtLoginName.Text.Trim()),
                   new SqlParameter("@UserName",txtUserName.Text.Trim()),
                   new SqlParameter("@RoleId",Convert.ToInt32(ddlRole.SelectedValue))
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editOk", "alert('修改成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editError", "alert('修改失败！')", true);
                }
            }
        }

        private bool CheckLoginName(string p)
        {
            if (p == "add")
            {
                string sql = "select Id from tb_UserInfo where LoginName=@LoginName";
                SqlParameter parameter = new SqlParameter("@LoginName", txtLoginName.Text.Trim());
                object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql, parameter);
                if (obj != null)
                {
                    return false;
                }
            }
            else
            {
                string sql = "select Id from tb_UserInfo where LoginName=@LoginName and Id<>" + Request["id"];
                SqlParameter parameter = new SqlParameter("@LoginName", txtLoginName.Text.Trim());
                object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql, parameter);
                if (obj != null)
                {
                    return false;
                }
            }
            return true;
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserManage.aspx");
        }
    }
}