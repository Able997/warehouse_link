﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class StudentScoreAdd : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                btnOk.Attributes.Add("OnClick", "return checkStuScore();");
                BindSubject();
                BindSchedule();
                if (Request["id"] != null)
                {
                    lableInfo.Text = "学生成绩修改";
                    btnOk.Text = "修 改";
                    txtNum.Enabled = false;
                    BindScore(Request["id"].ToString());
                }
            }
        }

        private void BindScore(string id)
        {
            string sql = "select a.*,b.StuNum from tb_Score a join tb_StuInfo b on a.StuId=b.Id where a.Id= " + id;
            SqlDataReader sdr = SqlHelper.ExecuteReader(connStr, CommandType.Text, sql);
            if (sdr.Read())
            {
                txtNum.Text = sdr["StuNum"].ToString();
                ddlSchedule.SelectedValue = sdr["ScheduleId"].ToString();
                ddlSubject.SelectedValue = sdr["SubjectId"].ToString();
                txtScore.Text = sdr["Score"].ToString();
            }
            sdr.Close();
        }

        private void BindSchedule()
        {
            string sql = "select * from tb_Schedule";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            ddlSchedule.DataValueField = "Id";
            ddlSchedule.DataTextField = "ScheduleName";
            ddlSchedule.DataSource = ds;
            ddlSchedule.DataBind();
        }

        private void BindSubject()
        {
            string sql = "select * from tb_Subject";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            ddlSubject.DataValueField = "Id";
            ddlSubject.DataTextField = "SubjectName";
            ddlSubject.DataSource = ds;
            ddlSubject.DataBind();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            /************************************************/
            

            //判断科目、期次是否有值
            if (ddlSchedule.SelectedValue == "" || ddlSubject.SelectedValue == "")
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "noerror", "alert('科目或期次不能为空！')", true);
                return;
            }
            /*************************************************/
            double stuScore;
            try
            {
                stuScore = Convert.ToDouble(txtScore.Text.Trim());
            }
            catch
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "scorerror", "alert('成绩输入错误！')", true);
                return;
            }
            //添加
            if (btnOk.Text == "添 加")
            {
                //判断学号是否存在
                string chkNumSql = "select Id from tb_StuInfo where StuNum=@StuNum";
                SqlParameter parameterNum = new SqlParameter("@StuNum", txtNum.Text.Trim());
                object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, chkNumSql, parameterNum);
                if (obj == null)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "chkError", "alert('该学号不存在！')", true);
                    return;
                }
                string stuId = obj.ToString();
                //判断是否重复录入成绩
                string chkReSql = "select Id from tb_Score where StuId=" + stuId + " and SubjectId=" + ddlSubject.SelectedValue + " and ScheduleId=" + ddlSchedule.SelectedValue;
                object obj2 = SqlHelper.ExecuteScalar(connStr, CommandType.Text, chkReSql);
                if (obj2 != null)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "reerror", "alert('不可重复输入同学号、同科目、同期次！')", true);
                    return;
                }
                string sql = @"insert into tb_Score values(@stuId,@SubjectId," + stuScore + ",@ScheduleId)";
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@stuId",stuId),
                   new SqlParameter("@SubjectId",ddlSubject.SelectedValue),
                   new SqlParameter("@ScheduleId",ddlSchedule.SelectedValue)
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addOk", "alert('添加成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addError", "alert('添加失败！')", true);
                }
            }
            //修改
            else
            {
                string sql = "update tb_Score set SubjectId=@SubjectId,ScheduleId=@ScheduleId,Score="+stuScore+" where Id=" + Request["id"];

                SqlParameter[] parameters = 
                {
                   new SqlParameter("@SubjectId",ddlSubject.SelectedValue),
                   new SqlParameter("@ScheduleId",ddlSchedule.SelectedValue)
                };
                bool result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editOk", "alert('修改成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editError", "alert('修改失败！')", true);
                }
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentScore.aspx");
        }
    }
}