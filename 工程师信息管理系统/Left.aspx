﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Left.aspx.cs" Inherits="SMSystem.Left" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        body {
            margin: 0;
            text-align: center;
            color: #000;
            font:12px;
            font-family:'Microsoft YaHei',FangSong;
            background: #B9D890;
            line-height: 150%;
        }
       
        a:link, a:visited {
            color: #385065;
            text-decoration: none;
            outline:none;
        }

        a:hover {
            text-decoration: underline;
        }

        #menu {
            width: 167px;
            margin: 0px 3px;
            padding: 0px;
            text-align: left;
            list-style: none;
        }

        #menu .item {
            margin: 0px 0px;
            padding:0px;
            list-style: none;
            background-image: url(Images/menuback1111.jpg);
        }

        a.title:link, a.title:visited, a.title:hover {
            display: block;
            background: url(/images/bg002.jpg) no-repeat;
            color: #385065;
            font-weight: bold;
            padding: 0 0 0 22px;
            width: 142px;
            line-height: 23px;
            cursor: pointer;
            text-decoration: none;
        }

        #menu .item ul {
            border: 1px solid #9FACB7;
            margin: 0;
            width: 135px;
            padding: 3px 0px 3px 30px;
            background: #fff;
            list-style: none;
            display: none;
        }

            #menu .item ul li {
                display: block;
            }
    </style>
    <script language="javascript" type="text/javascript">
        // --- 获取ClassName
        document.getElementsByClassName = function (cl) {
            var retnode = [];
            var myclass = new RegExp('\\b' + cl + '\\b');
            var elem = this.getElementsByTagName('*');
            for (var j = 0; j < elem.length; j++) {
                var classes = elem[j].className;
                if (myclass.test(classes)) retnode.push(elem[j]);
            }
            return retnode;
        }

        // --- 隐藏所有
        function HideAll() {
            var items = document.getElementsByClassName("optiton");
            for (var j = 0; j < items.length; j++) {
                items[j].style.display = "none";
            }
        }

        // --- 设置cookie
        function setCookie(sName, sValue, expireHours) {
            var cookieString = sName + "=" + escape(sValue);
            //;判断是否设置过期时间
            if (expireHours > 0) {
                var date = new Date();
                date.setTime(date.getTime + expireHours * 3600 * 1000);
                cookieString = cookieString + "; expire=" + date.toGMTString();
            }
            document.cookie = cookieString;
        }

        //--- 获取cookie
        function getCookie(sName) {
            var aCookie = document.cookie.split("; ");
            for (var j = 0; j < aCookie.length; j++) {
                var aCrumb = aCookie[j].split("=");
                if (escape(sName) == aCrumb[0])
                    return unescape(aCrumb[1]);
            }
            return null;
        }

        window.onload = function () {
            var show_item = "opt_1";
            if (getCookie("show_item") != null) {
                show_item = "opt_" + getCookie("show_item");
            }
            document.getElementById(show_item).style.display = "block";
            var items = document.getElementsByClassName("title");
            for (var j = 0; j < items.length; j++) {
                items[j].onclick = function () {
                    var o = document.getElementById("opt_" + this.name);
                    if (o.style.display != "block") {
                        HideAll();
                        o.style.display = "block";
                        setCookie("show_item", this.name);
                    }
                    else {
                        o.style.display = "none";
                    }
                }
            }
        }
    </script>
</head>
<body>
    <div style="background-image: url(Images/backleft.jpg); height: 22px; width: 170px; border-right: 2px solid #B9D890; color: white;font-weight: 700; padding-bottom: 3px;text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;系统菜单</div>
    <div style="background-color: #B9D890;">
        <ul id="menu">
            <li class="item"><a href="javascript:void(0)" class="title" name="1">基本管理</a>
                <ul id="opt_1" class="optiton" runat="server">
                </ul>
            </li>
            <li class="item"><a href="javascript:void(0)" class="title" name="2">系统管理</a>
                <ul id="opt_2" class="optiton">
                    <li><a href="#" onclick="javascript:parent.window.open('', '_self', '');parent.window.close();">退出系统</a></li>
                </ul>
            </li>
           
        </ul>

    </div>
</body>
</html>

