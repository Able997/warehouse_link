﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class ScheduleAdd : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOk.Attributes.Add("OnClick", "return checkSchedule();");
            if (!IsPostBack)
            {
                if (Request["id"] != null)
                {
                    lableInfo.Text = "排期信息修改";
                    btnOk.Text = "修 改";
                    BindSchedule(Request["id"].ToString());
                }
            }
        }

        private void BindSchedule(string id)
        {
            string sql = "select * from tb_Schedule where Id=" + id;
            SqlDataReader sdr = SqlHelper.ExecuteReader(connStr, CommandType.Text, sql);
            if (sdr.Read())
            {
                txtScheduleName.Text = sdr["ScheduleName"].ToString();
                txtRemark.Text = Server.HtmlDecode(sdr["Remark"].ToString());
            }
            sdr.Close();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            //添加
            if (btnOk.Text == "添 加")
            {
                string sql = "insert into tb_Schedule values(@ScheduleName,@Remark)";
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@ScheduleName",txtScheduleName.Text.Trim()),
                   new SqlParameter("@Remark",Server.HtmlEncode(txtRemark.Text.Trim()))
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addOk", "alert('添加成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addError", "alert('添加失败！')", true);
                }
            }
            //修改
            else
            {
                string sql = "update tb_Schedule set ScheduleName=@ScheduleName,Remark=@Remark where Id=" + Request["id"];

                SqlParameter[] parameters = 
                {
                   new SqlParameter("@ScheduleName",txtScheduleName.Text.Trim()),
                   new SqlParameter("@Remark",Server.HtmlEncode(txtRemark.Text.Trim()))
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editOk", "alert('修改成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editError", "alert('修改失败！')", true);
                }
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("ScheduleManage.aspx");
        }
    }
}