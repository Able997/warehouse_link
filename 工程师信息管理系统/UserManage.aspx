﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserManage.aspx.cs" Inherits="SMSystem.UserManage" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html>

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }

        .STYLE1 {
            font-size: 12px;
        }

        .STYLE4 {
            font-size: 12px;
            color: #1F4A65;
            font-weight: bold;
        }

        a:link {
            font-size: 12px;
            color: #06482a;
            text-decoration: none;
        }

        a:visited {
            font-size: 12px;
            color: #06482a;
            text-decoration: none;
        }

        a:hover {
            font-size: 12px;
            color: #FF0000;
            text-decoration: underline;
        }

        a:active {
            font-size: 12px;
            color: #FF0000;
            text-decoration: none;
        }

        #gvUserManage {
            width: 99%;
            border: 0px;
            text-align: center;
        }

            #gvUserManage th {
                border: 0px;
                background-image: url("Images/tab/tab_14.gif");
            }

            #gvUserManage td {
                border: 1px solid #FFFFFF;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td height="30">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="15">
                                <img src="Images/tab/tab_03.gif" width="15" height="30" /></td>
                            <td background="Images/tab/tab_05.gif">
                                <img src="Images/tab/311.gif" width="16" height="16" />
                                <span class="STYLE4">用户信息管理</span>
                                <table style="float: right;">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:Button ID="btnAdd" runat="server" Text="新增信息" OnClick="btnAdd_Click" /></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="14">
                                <img src="Images/tab/tab_07.gif" width="14" height="30" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="9" background="Images/tab/tab_12.gif">&nbsp;</td>
                            <td bgcolor="e5f1d6">

                                <asp:GridView ID="gvUserManage" runat="server" PageSize="10" AutoGenerateColumns="false" Width="100%" PageIndex="1" OnRowCommand="gvUserManage_RowCommand" OnRowDataBound="gvUserManage_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="登录名">
                                            <ItemTemplate>
                                                <%#Eval("LoginName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="姓名">
                                            <ItemTemplate>
                                                <%#Eval("UserName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="角色">
                                            <ItemTemplate>
                                                <%#Eval("RoleName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="操作" HeaderStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnUpdate" CommandName="upd" CommandArgument='<%#Eval("Id") %>' Visible='<%#Eval("RoleId").ToString()=="1" ? false : true %>' runat="server"><img src="Images/114.gif" style="border:0px;"/>编辑</asp:LinkButton>
                                                &nbsp;&nbsp;
                                            <asp:LinkButton ID="lbtnDelete" OnClientClick="return confirm('确定要删除吗？')" CommandName="del" CommandArgument='<%#Eval("Id")  %>' Visible='<%#Eval("RoleId").ToString()=="1" ? false : true %>' runat="server"><img src="Images/114.gif" style="border:0px;"/>删除</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>


                            </td>
                            <td width="9" background="Images/tab/tab_16.gif">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="29">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="15" height="29">
                                <img src="Images/tab/tab_20.gif" width="15" height="29" /></td>
                            <td background="Images/tab/tab_21.gif">

                                <webdiyer:AspNetPager runat="server" ID="AspNetPager1" runat="server" HorizontalAlign="Right" FirstPageText="<<" LastPageText=">>" PrevPageText="<" NextPageText=">" NumericButtonTextFormatString=" {0} " Width="600px"
                                    ShowCustomInfoSection="Left" ShowBoxThreshold="2" PageSize="5" InputBoxClass="text2" TextAfterInputBox="" OnPageChanged="AspNetPager1_PageChanged">
                                </webdiyer:AspNetPager>
                            </td>
                            <td width="14">
                                <img src="Images/tab/tab_22.gif" width="14" height="29" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
