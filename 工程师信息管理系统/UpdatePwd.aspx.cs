﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class UpdatePwd : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOk.Attributes.Add("OnClick", "return checkPwd();");
            if (Request["userId"] != null)
            {
                BindPwd();
            }
        }

        private void BindPwd()
        {
            string sql = "select LoginName from tb_UserInfo where Id=" + Request["userId"];
            SqlDataReader sdr = SqlHelper.ExecuteReader(connStr, CommandType.Text, sql);
            if (sdr.Read())
            {
                txtLoginName.Text = sdr["LoginName"].ToString();
            }
            sdr.Close();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            string oldPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(this.txtOldPwd.Text, "MD5");
            string sql = "select Id from tb_UserInfo where LoginName='" + txtLoginName.Text + "' and LoginPwd=@LoginPwd";
            SqlParameter parameter = new SqlParameter("@LoginPwd", oldPwd);
            object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql, parameter);
            if (obj == null)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "nothing", "alert('原密码不正确！')", true);
                return;
            }
            string newPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(this.txtNewPwd.Text, "MD5");
            string sql2 = "update tb_UserInfo set LoginPwd=@LoginPwd where LoginName='" + txtLoginName.Text + "'";
            SqlParameter parameter2 = new SqlParameter("@LoginPwd", newPwd);
            bool result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql2, parameter2) > 0 ? true : false;
            if (result)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "updOk", "alert('修改成功！')", true);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "updError", "alert('修改失败！')", true);
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}