﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.SessionState;

namespace SMSystem
{
    public class CheckPower
    {
        public bool ChkPower(string menuId)
        {
            bool result = false;
            HttpSessionState session = HttpContext.Current.Session;
            string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
            string sql = "select b.PowerIds from tb_UserInfo a join tb_Role b on a.RoleId=b.Id where a.Id="+session["UserId"];
            object obj = SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql);
            if (obj == null)
            {
                return false;
            }
            string[] urlIds = obj.ToString().Split(',');
            for (int i = 0; i < urlIds.Length-1; i++)
            {
                if (urlIds[i] == menuId)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
}