﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class UserManage : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindgvUser(gvUserManage.PageSize,gvUserManage.PageIndex);
            }
        }

        private void BindgvUser(int pagesize, int pageindex)
        {
            string sql0 = "select COUNT(*) from tb_UserInfo";
            AspNetPager1.AlwaysShow = true;
            AspNetPager1.PageSize = pagesize;

            string sql = "select top "+pagesize+" a.*,b.RoleName from tb_UserInfo a join tb_Role b on a.RoleId=b.Id where a.Id not in(select top "+(pageindex-1)*pagesize+" Id from tb_UserInfo)";
            AspNetPager1.RecordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql0));
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            gvUserManage.DataSource = ds;
            gvUserManage.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserAdd.aspx");
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvUserManage.PageIndex = AspNetPager1.CurrentPageIndex;
            BindgvUser(gvUserManage.PageSize, gvUserManage.PageIndex);
        }

        protected void gvUserManage_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void gvUserManage_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string id = e.CommandArgument.ToString();
            if (e.CommandName == "upd")
            {
                Response.Redirect("UserAdd.aspx?Id=" + id);
            }
            else
            {
                string sql = "delete from tb_UserInfo where Id=" + id;
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                    BindgvUser(gvUserManage.PageSize, gvUserManage.PageIndex);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
                }
            }
        }
    }
}