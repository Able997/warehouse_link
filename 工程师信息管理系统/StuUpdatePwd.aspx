﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StuScore.Master" AutoEventWireup="true" CodeBehind="StuUpdatePwd.aspx.cs" Inherits="SMSystem.StuUpdatePwd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .tableframe {
            margin-left: 4px;
            width: 346px;
            border: solid #add9c0;
            border-width: 1px 0px 0px 1px;
        }
        .tableframe td {
            border: solid #add9c0;
            border-width: 0px 1px 1px 0px;
            padding: 5px 0px;
        }

        .tdleft {
            width: 100px;
            text-align: right;
            background-color: #EBFBD6;
            color: #1F4A65;
        }
    </style>
    <script type="text/javascript">
        function checkPwd() {
            if (document.getElementById("<%=txtNewPwd.ClientID%>").value == "" || document.getElementById("<%=txtNewPwd.ClientID%>").value.indexOf(" ") != -1) {
                alert('新密码不能为空！');
                return false;
            }
            if (document.getElementById("<%=txtNewPwd.ClientID%>").value != document.getElementById("<%=txtRPwd.ClientID%>").value) {
                alert('两次密码不一致！');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width: 100%; height: 490px;">
                        <br />
                        <br />
                        <br />
                        <table border="0" style="width: 350px; margin: 80px auto;" cellpadding="0" border="0" cellspacing="0">
                <tr>
                    <td style="background-image: url(Images/tab/tab_03.gif); height: 30px; width: 11px;"></td>
                    <td style="background-image: url(Images/tab/tab_05.gif); width: 350px; height: 30px; font-size: 12px; color: #1F4A65; font-weight: bold;">
                        <img src="Images/tab/311.gif" width="16" height="16" />&nbsp;&nbsp;<asp:Label ID="lableInfo" runat="server" Text="密码修改"></asp:Label>

                    </td>
                    <td style="background-image: url(Images/tab/tab_07.gif); height: 30px; width: 11px;"></td>
                </tr>
                <tr>

                    <td colspan="3">
                        <table class="tableframe" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tdleft">新密码：</td>
                                <td>
                                    <asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password" MaxLength="20" Width="150px"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="tdleft">确认密码：</td>
                                <td>
                                    <asp:TextBox ID="txtRPwd" runat="server" TextMode="Password" MaxLength="20" Width="150px"></asp:TextBox></td>
                            </tr>
                            
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnOk" runat="server" Text="修 改" OnClick="btnOk_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnReturn" runat="server" Text="返 回" OnClick="btnReturn_Click" />
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
                    </div>
</asp:Content>
