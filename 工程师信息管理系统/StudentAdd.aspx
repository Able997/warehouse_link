﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentAdd.aspx.cs" Inherits="SMSystem.StudentAdd" %>

<!DOCTYPE html>


<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="Scripts/calendar.js"></script>
    <style>
        body {
            background-color:#F2FFE2;
        }
        .tableframe {
            margin-left: 4px;
            width: 346px;
            border: solid #add9c0;
            border-width: 1px 0px 0px 1px;
        }
        .tableframe td {
            border: solid #add9c0;
            border-width: 0px 1px 1px 0px;
            padding: 5px 0px;
        }

        .tdleft {
            width: 100px;
            text-align: right;
            background-color: #EBFBD6;
            color: #1F4A65;
        }
    </style>
    <script type="text/javascript">
        function checkStuInfo() {
            if (document.getElementById("<%=txtstuId.ClientID%>").value == "" || document.getElementById("<%=txtstuId.ClientID%>").value.indexOf(" ") != -1) {
                alert('工号不能为空！');
                return false;
            }
            if (document.getElementById("<%=txtstuName.ClientID%>").value == "" || document.getElementById("<%=txtstuName.ClientID%>").value.indexOf(" ") != -1){
                alert('姓名不能为空！');
                return false;
            }
            if (document.getElementById("<%=txtBirth.ClientID%>").value == ""|| document.getElementById("<%=txtBirth.ClientID%>").value.indexOf(" ") != -1) {
                alert('出生日期不能为空！');
                return false;
            }
            if (document.getElementById("<%=txtJionTime.ClientID%>").value == ""|| document.getElementById("<%=txtJionTime.ClientID%>").value.indexOf(" ") != -1) {
                alert('入职时间不能为空！');
                return false;
            }
            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" style="width: 350px; margin: 80px auto;" cellpadding="0" border="0" cellspacing="0">
                <tr>
                    <td style="background-image: url(Images/tab/tab_03.gif); height: 30px; width: 11px;"></td>
                    <td style="background-image: url(Images/tab/tab_05.gif); width: 350px; height: 30px; font-size: 12px; color: #1F4A65; font-weight: bold;">
                        <img src="Images/tab/311.gif" width="16" height="16" />&nbsp;&nbsp;<asp:Label ID="lableInfo" runat="server" Text="工程师信息添加"></asp:Label>

                    </td>
                    <td style="background-image: url(Images/tab/tab_07.gif); height: 30px; width: 11px;"></td>
                </tr>
                <tr>

                    <td colspan="3">
                        <table class="tableframe" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="tdleft">工号：</td>
                                <td>
                                    <asp:TextBox ID="txtstuId" runat="server" MaxLength="20" ></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="tdleft">密码：</td>
                                <td>
                                    <asp:TextBox ID="txtstuPwd" runat="server" MaxLength="20" Width="150px" TextMode="Password" ></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="tdleft">姓名：</td>
                                <td>
                                    <asp:TextBox ID="txtstuName" runat="server" MaxLength="10"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="tdleft">性别：</td>
                                <td>
                                    <asp:RadioButton ID="radMan" runat="server" Checked="true" GroupName="sex" />男
                                    <asp:RadioButton ID="radWoman" runat="server" GroupName="sex" />
                                女
                            </tr>
                            <tr>
                                <td class="tdleft">出生日期：</td>
                                <td>
                                    <input name="date" type="text" id="txtBirth" onclick="new Calendar().show(this);" width="150px" readonly="readonly" runat="server"/></td>
                            </tr>
                            
                            <tr>
                                <td class="tdleft">部门：</td>
                                <td>
                                    <asp:DropDownList ID="ddlClass" runat="server" Width="153px"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdleft">入职时间：</td>
                                <td>
                                    <input name="date" type="text" id="txtJionTime" onclick="new Calendar().show(this);" width="150px" readonly="readonly" runat="server"/></td>
                            </tr>

                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <asp:Button ID="btnOk" runat="server" Text="添 加" OnClick="btnOk_Click" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnReturn" runat="server" Text="返 回" OnClick="btnReturn_Click" />
                                </td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
        </div>
    </form>
</body>
</html>
