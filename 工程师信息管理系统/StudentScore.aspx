﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentScore.aspx.cs" Inherits="SMSystem.StudentScore" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html>

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }

        .STYLE1 {
            font-size: 12px;
        }

        .STYLE4 {
            font-size: 12px;
            color: #1F4A65;
            font-weight: bold;
        }

        a:link {
            font-size: 12px;
            color: #06482a;
            text-decoration: none;
        }

        a:visited {
            font-size: 12px;
            color: #06482a;
            text-decoration: none;
        }

        a:hover {
            font-size: 12px;
            color: #FF0000;
            text-decoration: underline;
        }

        a:active {
            font-size: 12px;
            color: #FF0000;
            text-decoration: none;
        }

        #gvStudentScore {
            width: 99%;
            border: 0px;
            text-align: center;
        }

            #gvStudentScore th {
                border: 0px;
                background-image: url("Images/tab/tab_14.gif");
            }

            #gvStudentScore td {
                border: 1px solid #FFFFFF;
            }
    </style>
    <script type="text/javascript">
        function checkDelete(checkBoxName) {
            var xx = "";
            var checkBox = document.getElementsByName(checkBoxName);
            for (var i = 0; i < checkBox.length; i++) {
                if (checkBox[i].checked) {
                    xx += checkBox[i].value + ",";
                }
            }
            if (xx == "") {
                alert('请选择要删除的行！');
                return false;
            }
            document.getElementById('txtScore').value = xx.substring(0, xx.length - 1);
            return confirm('确定要删除选定的信息吗？');
        }

        //全选
        function checkAll(checkBoxName) {
            var checkBox = document.getElementsByName(checkBoxName);
            for (var i = 0; i < checkBox.length; i++) {
                checkBox[i].checked = document.getElementById("chkAll").checked;
            }
        }

        function getCheck(checkBoxName) {
            var xx = "";
            var checkBox = document.getElementsByName(checkBoxName);
            for (var i = 0; i < checkBox.length; i++) {
                if (checkBox[i].checked) {
                    xx += checkBox[i].value + ",";
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            
            <tr>
                <td height="30">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">


                        <tr>

                            <td background="Images/tab/tab_05.gif">

                                <div style="min-width: 800px; margin: auto; height: 30px">
                                    <img src="Images/tab/311.gif" width="16" height="16" />
                                    <span class="STYLE4">工程师工资管理</span><span style="float: right; margin-top: 5px">
                                    工号：<asp:TextBox ID="txtStuNum" runat="server" Width="65px"></asp:TextBox>&nbsp;&nbsp;
                                  姓名：<asp:TextBox ID="txtName" runat="server" Width="65px"></asp:TextBox>&nbsp;&nbsp;
                                 工程期次：<asp:TextBox ID="txtSchedule" runat="server" Width="65px"></asp:TextBox>&nbsp;&nbsp;
                               部门：<asp:TextBox ID="txtClass" runat="server" Width="65px"></asp:TextBox>&nbsp;&nbsp;
                              工程：<asp:TextBox ID="txtSubject" runat="server" Width="65px"></asp:TextBox>&nbsp;&nbsp;
                                        <asp:Button ID="btnSearch" runat="server" Text="查 询" Height="22px" OnClick="btnSearch_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 </span>
                                </div>

                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="9" background="Images/tab/tab_12.gif">&nbsp;</td>
                            <td bgcolor="e5f1d6">

                                <asp:GridView ID="gvStudentScore" runat="server" PageSize="10" AutoGenerateColumns="false" Width="100%" OnRowDataBound="gvStudentScore_RowDataBound" PageIndex="1" OnRowCommand="gvStudentScore_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Width="30px">
                                            <HeaderTemplate>
                                                <input type="checkbox" id="chkAll" class="chkAll" onclick="checkAll('Singlechk')" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <input type="checkbox" id='chkSingle<%#Eval("Id") %>' name="Singlechk" value='<%#Eval("Id") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="工号">
                                            <ItemTemplate>
                                                <%#Eval("StuNum") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="姓名">
                                            <ItemTemplate>
                                                <%#Eval("StuName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="工程期次">
                                            <ItemTemplate>
                                                <%#Eval("ScheduleName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="部门">
                                            <ItemTemplate>
                                                <%#Eval("ClassName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="工程名称">
                                            <ItemTemplate>
                                                <%#Eval("SubjectName") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="工资">
                                            <ItemTemplate>
                                                <%#Eval("Score") %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="操作" HeaderStyle-Width="150px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbtnUpdate" CommandName="upd" CommandArgument='<%#Eval("Id") %>' runat="server"><img src="Images/114.gif" style="border:0px;"/>编辑</asp:LinkButton>
                                                &nbsp;&nbsp;
                                            <asp:LinkButton ID="lbtnDelete" OnClientClick="return confirm('确定要删除吗？')" CommandName="del" CommandArgument='<%#Eval("Id") %>' runat="server"><img src="Images/114.gif" style="border:0px;"/>删除</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>


                            </td>
                            <td width="9" background="Images/tab/tab_16.gif">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="29">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="15" height="29">
                                <img src="Images/tab/tab_20.gif" width="15" height="29" /></td>
                            <td background="Images/tab/tab_21.gif">

                                <webdiyer:AspNetPager runat="server" ID="AspNetPager1" runat="server" HorizontalAlign="Right" FirstPageText="<<" LastPageText=">>" PrevPageText="<" NextPageText=">" NumericButtonTextFormatString=" {0} " Width="600px"
                                    ShowCustomInfoSection="Left" ShowBoxThreshold="2" PageSize="5" InputBoxClass="text2" TextAfterInputBox="" OnPageChanged="AspNetPager1_PageChanged">
                                </webdiyer:AspNetPager>
                            </td>
                            <td width="14">
                                <img src="Images/tab/tab_22.gif" width="14" height="29" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        &nbsp;&nbsp;
         <asp:Button ID="btnAdd" runat="server" Text="新增信息" OnClick="btnAdd_Click"  />&nbsp;&nbsp;
        <asp:Button ID="btnOut" runat="server" Text="导出Excel" OnClick="btnOut_Click"   />&nbsp;&nbsp;
         <asp:Button ID="btnDelete" runat="server" Text="批量删除" OnClick="btnDelete_Click"   />
        <br /><input id="txtScore" type="hidden" runat="server" />
    </form>
</body>
</html>
