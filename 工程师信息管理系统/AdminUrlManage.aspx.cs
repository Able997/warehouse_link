﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class AdminUrlManage : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindAdminUrl(gvAdminUrl.PageSize, gvAdminUrl.PageIndex);
            }
        }

        private void BindAdminUrl(int pagesize, int pageindex)
        {
            string sql0 = "select count(*) from tb_Menu";
            string sql = "select top " + pagesize + " * from tb_Menu where Id not in(select top " + (pageindex - 1) * pagesize + " Id from tb_Menu)";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            AspNetPager1.AlwaysShow = true;
            AspNetPager1.PageSize = pagesize;
            AspNetPager1.RecordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql0));
            gvAdminUrl.DataSource = ds;
            gvAdminUrl.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AdminUrlAdd.aspx");
        }

        protected void gvAdminUrl_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void gvAdminUrl_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string urlId = e.CommandArgument.ToString();
            if (e.CommandName == "upd")
            {
                Response.Redirect("AdminUrlAdd.aspx?urlId=" + urlId);
            }
            else
            {
                string sql = "delete from tb_Menu where Id=" + urlId;
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                    BindAdminUrl(gvAdminUrl.PageSize, gvAdminUrl.PageIndex);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
                }
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            gvAdminUrl.PageIndex = AspNetPager1.CurrentPageIndex;
            BindAdminUrl(gvAdminUrl.PageSize, gvAdminUrl.PageIndex);
        }
    }
}