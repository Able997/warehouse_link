﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class StuScoreSearch : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //判断cookeis
                if (Request.Cookies["StuId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                BindSchedule();
                BindSubject();
                BindScore();
                HttpCookie cookie2 = Request.Cookies["StuName"];
                labLoginer.Text = "<strong>" + HttpUtility.UrlDecode(cookie2.Value) + "的成绩信息</strong>";
            }
        }

        private void BindSubject()
        {
            string sql = "select * from tb_Subject";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            ddlSubject.DataValueField = "Id";
            ddlSubject.DataTextField = "SubjectName";
            ddlSubject.DataSource = ds;
            ddlSubject.DataBind();
            ddlSubject.Items.Insert(0, new ListItem("  ", "0"));
        }

        private void BindSchedule()
        {
            string sql = "select * from tb_Schedule";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            ddlSchedule.DataValueField = "Id";
            ddlSchedule.DataTextField = "ScheduleName";
            ddlSchedule.DataSource = ds;
            ddlSchedule.DataBind();
            ddlSchedule.Items.Insert(0, new ListItem("  ", "0"));
        }

        private void BindScore()
        {
            HttpCookie cookie = Request.Cookies["StuId"];
            string cookStuid = cookie.Value;
            StringBuilder str = new StringBuilder();
            str.Append("select a.Score,b.SubjectName,c.ScheduleName from tb_Score a join tb_Subject b on a.SubjectId=b.Id join tb_Schedule c on c.Id=a.ScheduleId where a.StuId=" + cookStuid);
            if (ddlSubject.SelectedIndex > 0)
            {
                str.Append(" and a.SubjectId=" + ddlSubject.SelectedValue);
            }
            if (ddlSchedule.SelectedIndex > 0)
            {
                str.Append(" and a.ScheduleId=" + ddlSchedule.SelectedValue);
            }
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, str.ToString());
            gvScore.DataSource = ds;
            gvScore.DataBind();
        }

        protected void gvScore_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void gvScore_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvScore.PageIndex = e.NewPageIndex;
            BindScore();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindScore();
        }
    }
}