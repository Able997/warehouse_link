﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class Left : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindMenu();
            }
        }

        private void BindMenu()
        {
            
            object obj= SqlHelper.ExecuteScalar(connStr, CommandType.Text, sql);
            if (obj != null)
            {
                if (obj.ToString().Length > 0)
                {
                    string sqlPower = "select * from tb_Menu where Id in(" + obj.ToString().Substring(0, obj.ToString().Length - 1) + ")";
                    DataTable dt = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sqlPower).Tables[0];
                    StringBuilder str = new StringBuilder();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        str.Append("<li><a href='" + dt.Rows[i]["MenuUrl"].ToString() + "?menuId=" + dt.Rows[i]["Id"].ToString() + "' target='mainFrame'>" + dt.Rows[i]["MenuName"].ToString() + "</a></li>");
                    }
                    opt_1.InnerHtml = str.ToString(); ;
                }
            }
        }
    }
}