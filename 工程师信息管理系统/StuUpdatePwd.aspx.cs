﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class StuUpdatePwd : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOk.Attributes.Add("OnClick", "return checkPwd();");
            if (!IsPostBack)
            {
                //判断cookeis
                if (Request.Cookies["StuId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            HttpCookie cookie = Request.Cookies["StuId"];
            string cookStuid = cookie.Value;
            string sql = "update tb_StuInfo set StuPwd=@StuPwd where Id=" + cookStuid;
            SqlParameter parameter = new SqlParameter("@StuPwd", txtNewPwd.Text.Trim());
            bool result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameter) > 0 ? true : false;
            if (result)
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "updOk", "alert('修改成功！');location.href='StuScoreSearch.aspx'", true);
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "updError", "alert('修改失败！')", true);
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("StuScoreSearch.aspx");
        }
    }
}