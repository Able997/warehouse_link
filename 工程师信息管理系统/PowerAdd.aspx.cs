﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class PowerAdd : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnOk.Attributes.Add("OnClick", "return checkGroup('powerchk');");
            if (!IsPostBack)
            {
                if (Request["roleId"] != null)
                {
                    lableInfo.Text = "角色权限修改";
                    btnOk.Text = "修 改";
                    BindRole(Request["roleId"].ToString());
                }
                BindPower();
            }
        }

        private void BindPower()
        {
            string sql = "select * from tb_Menu";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            string Id = "";
            string[] power = null;
            if (txtPower.Value.Length != 0)
            {
                power=txtPower.Value.Split(',');
            }
            StringBuilder str = new StringBuilder();
            str.Append("<table class='tablepower'>");
            int count = ds.Tables[0].Rows.Count;
            for (int i = 0; i < count; i++)
            {
                if (i % 3 == 0)
                {
                    str.Append("<tr>");
                    if (i < count)
                    {
                        Id = ds.Tables[0].Rows[i]["Id"].ToString();
                        str.Append("<td><input id='Checkbox" + Id + "' type='checkbox' name='powerchk' value='" + Id + "'");
                        if (power != null)
                        {
                            for (int j = 0; j < power.Length-1; j++)
                            {
                                if (Id == power[j])
                                {
                                    str.Append(" checked='checked'");
                                }
                            }
                        }
                        str.Append("/>");
                        str.Append(ds.Tables[0].Rows[i]["MenuName"].ToString());
                        str.Append("</td>");
                    }
                    else
                    {
                        break;
                    }
                    if (i < count - 1)
                    {
                        Id = ds.Tables[0].Rows[i + 1]["Id"].ToString();
                        str.Append("<td><input id='Checkbox" + Id + "' type='checkbox' name='powerchk' value='" + Id + "'");
                        if (power != null)
                        {
                            for (int j = 0; j < power.Length - 1; j++)
                            {
                                if (Id == power[j])
                                {
                                    str.Append(" checked='checked'");
                                }
                            }
                        }
                        str.Append("/>");
                        str.Append(ds.Tables[0].Rows[i + 1]["MenuName"].ToString());
                        str.Append("</td>");
                    }
                    else
                    {
                        break;
                    }
                    if (i < count - 2)
                    {
                        Id = ds.Tables[0].Rows[i + 2]["Id"].ToString();
                        str.Append("<td><input id='Checkbox" + Id + "' type='checkbox' name='powerchk' value='" + Id + "'");
                        if (power != null)
                        {
                            for (int j = 0; j < power.Length - 1; j++)
                            {
                                if (Id == power[j])
                                {
                                    str.Append(" checked='checked'");
                                }
                            }
                        }
                        str.Append("/>");
                        str.Append(ds.Tables[0].Rows[i + 2]["MenuName"].ToString());
                        str.Append("</td>");
                        str.Append("</tr>");
                    }
                    else
                    {
                        break;
                    }
                }
            }
            str.Append("</table>");
            divpower.InnerHtml = str.ToString();
        }

        private void BindRole(string id)
        {
            string sql = "select * from tb_Role where Id=" + id ;
            SqlDataReader sdr = SqlHelper.ExecuteReader(connStr, CommandType.Text, sql);
            if (sdr.Read())
            {
                txtName.Text = sdr["RoleName"].ToString();
                txtRemark.Text =Server.HtmlDecode(sdr["Remark"].ToString());
                txtPower.Value = sdr["PowerIds"].ToString().Trim();
            }
            sdr.Close();
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            //添加
            if (btnOk.Text == "添 加")
            {
                string sql = "insert into tb_Role values(@RoleName,@PowerIds,@Remark)";
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@RoleName",this.txtName.Text.Trim()),
                   new SqlParameter("@PowerIds",this.txtPower.Value.Trim()),
                   new SqlParameter("@Remark",Server.HtmlEncode(txtRemark.Text.Trim()))
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addOk", "alert('添加成功！')", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "addError", "alert('添加失败！')", true);
                }
            }
            //修改
            else
            {
                string sql = "update tb_Role set RoleName=@RoleName,PowerIds=@PowerIds,Remark=@Remark where Id=" + Request["roleId"];
                SqlParameter[] parameters = 
                {
                   new SqlParameter("@RoleName",this.txtName.Text.Trim()),
                   new SqlParameter("@PowerIds",this.txtPower.Value.Trim()),
                   new SqlParameter("@Remark",Server.HtmlEncode(txtRemark.Text.Trim()))
                };
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql, parameters) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editOk", "alert('修改成功！');location.href='PowerManage.aspx'", true);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "editError", "alert('修改失败！')", true);
                }
            }
        }

        protected void btnReturn_Click(object sender, EventArgs e)
        {
            Response.Redirect("PowerManage.aspx");
        }
    }
}