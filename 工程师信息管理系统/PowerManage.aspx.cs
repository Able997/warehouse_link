﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMSystem
{
    public partial class PowerManage : System.Web.UI.Page
    {
        string connStr = ConfigurationManager.ConnectionStrings["SysConnstr"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["menuId"] == null || Session["UserId"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                CheckPower cp = new CheckPower();
                if (!cp.ChkPower(Request["menuId"].ToString()))
                {
                    Response.Redirect("Login.aspx");
                }
                BindGroup(gvPowerGroup.PageSize,gvPowerGroup.PageIndex);
            }
        }

        private void BindGroup(int pagesize,int pageindex)
        {
            string sql = "select  * from tb_Role";
            DataSet ds = SqlHelper.ExecuteDataset(connStr, CommandType.Text, sql);
            gvPowerGroup.DataSource = ds;
            gvPowerGroup.DataBind();
        }

        protected void gvPowerGroup_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //鼠标经过时，行背景色变 
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#B9D890'");
                //鼠标移出时，行背景色变 
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#E5F1D6'");
            }
        }

        protected void gvPowerGroup_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string roleId = e.CommandArgument.ToString();
            if (e.CommandName == "upd")
            {
                Response.Redirect("PowerAdd.aspx?roleId=" + roleId);
            }
            else
            {
                string sql = "delete from tb_Role where Id=" + roleId ;
                bool result = false;
                result = SqlHelper.ExecuteNonQuery(connStr, CommandType.Text, sql) > 0 ? true : false;
                if (result)
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delOk", "alert('删除成功！')", true);
                    BindGroup(gvPowerGroup.PageSize, gvPowerGroup.PageIndex);
                }
                else
                {
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "delError", "alert('删除失败！')", true);
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("PowerAdd.aspx");
        }
    }
}